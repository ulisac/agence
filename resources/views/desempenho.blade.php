@extends('app')

@section('menu') 
	@include('menu') 
@endsection


@section('cuerpo')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <h1 class="h2">Dashboard</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                <button type="button" class="btn btn-sm btn-outline-secondary active btn-primary"> Por Consultor</button>
                <button type="button" class="btn btn-sm btn-outline-secondary  disabled btn-primary">Por Cliente</button>
              </div>
              
            </div>
          </div>
          {{ Form::open(array('url' => '/home', 'id' => 'form1')) }}
          <input type="hidden" name="opc" id="opc" value="0">
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label>Periodo</label>
                <input type="text" autocomplete="off" name="daterange"  id="daterange" required="required" class="form-control"  value="<?= $p ?>"/>
              </div>
            </div>
            <div class="col-6 text-right">
              <div class="btn-group btn-group-lg">
                <button type="button" id="r" class="btn <?= $opc == 1 ? 'active' : '' ?>  btn-sm btn-outline-secondary">Relatorio</button>
                <button type="button" id="g" class="btn btn-sm <?= $opc == 2 ? 'active' : '' ?>  btn-outline-secondary">Grafico</button>
                <button type="button" id="p" class="btn btn-sm  <?= $opc == 3 ? 'active' : '' ?>  btn-outline-secondary">Pizza</button>
              </div>
            </div>            
           
            <div class="col-12">
              <div class="form-group">
                <label>Consultores</label>
                <select multiple="multiple" required="required" class="col-md-12 form-control" id="my-select" name="usuarios[]" > 
                  <?php foreach ($usuarios as $usuario ) { ?>
                               
                    <option <?php 
                      foreach ($seleccionados as $sel ) {
                        if($sel == $usuario['co_usuario']){echo "selected='selected'";}

                      }
                    ?> value="<?= $usuario['co_usuario'] ?>"><?=  $usuario['co_usuario']  ?> </option>  
                  <?php } ?>                
                </select>
              </div>
            </div>
          </div>
          {{ Form::close() }}
          
          <?php if(count($periodos)  > 0 ){ ?>
          <?php if($opc == 1 ){ ?>

          <h2>Relatorio</h2>
          <?php foreach ($periodos as $periodo ) { ?>
          <?php if(count($periodo['data'])  > 0 ){ ?>
                <div class="table-responsive">
                  <table class="table table-striped table-sm">
                    <thead>
                      <tr>
                        <th colspan="5"><?= $periodo['name'] ?> </th>
                      </tr>
                      <tr>
                        <th>Período</th>
                        <th>Receita Líquida</th>
                        <th>Custo Fixo</th>
                        <th>Comissão</th>
                        <th>Lucro</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $liquides = $costo_fijo = $comision = $lucro = 0; ?>
                      <?php foreach ($periodo['data'] as $valor ) { ?>
                      <?php
                        $liquides += $valor['liquides'];
                        $costo_fijo += $valor['costo_fijo'];
                        $comision += $valor['comision'];
                        $lucro += $valor['lucro']; 
                      ?>
                      <tr>
                        <td> <?= $valor['mes'] ?></td>
                        <td> <?= number_format($valor['liquides'],2) ?></td>
                        <td> <?= number_format($valor['costo_fijo'],2) ?></td>
                        <td> <?= number_format($valor['comision'],2) ?></td>
                        <td> <?= number_format($valor['lucro'],2) ?></td>
                      </tr>
                     <?php } ?>
                      <tr>
                        <th>&nbsp;</th>
                        <th><?= number_format($liquides,2)  ?> </th>
                        <th><?= number_format($costo_fijo,2)  ?> </th>
                        <th><?= number_format($comision,2)  ?> </th>
                        <th><?= number_format($lucro,2)  ?> </th>
                      </tr>
                    </tbody>
                  </table>
                </div>
             <?php } ?>
             <?php } ?>
         <?php }// fin del if ?>
         <?php if($opc == 2 ){ $type = "bar"; ?>
         <h2>Graficos </h2>
          <?php  $datt = array(); $prom = array(); $k = 0; $todos =  array(); $salario = 0;
          foreach ($periodos as $periodo ) {  ?>
          <?php if(count($periodo['data'])  > 0 ){ $k++; ?>
          <?php
            $labels = array(); 
            $data_liquides = array(); 
            $data_lucro = array(); 
          ?>

                      <?php $liquides = $costo_fijo = $comision = $lucro = 0; $salario = $salario + $periodo['salario']; ?>
                      <?php $j = 0; foreach ($periodo['data'] as $valor ) { 
                        array_push($labels, $valor['mes']);
                        array_push($data_liquides, $valor['liquides']);
                        $j++;
                        $liquides += $valor['liquides'];
                        $lucro += $valor['lucro']; 
                      ?>
                     <?php } ?>
                     <?php  array_push($todos, array(
                      'name' =>$periodo['name']  , 
                      'salario' =>$periodo['salario']  , 
                      'labels' => $labels ,
                     'data_liquides' => $data_liquides,
                      ))  ?>
                     <?php 
                        array_push($datt ,
                        array(
                          'label' => $periodo['name'],
                          'data' => $data_liquides,
                          'lineTension' => 0 ,
                          'backgroundColor' => 'transparent',
                          'borderColor' => "#".$periodo['color'],
                          'borderWidth' => 3,
                          'pointBackgroundColor' => '#000000'
                            )
                        );
                     
                      ?>
             <?php } ?>
             <?php } ?>
            
             <?php
             for ($i=0; $i < $j ; $i++) {
              array_push($prom , $salario / $k);
              }

              array_push($datt ,
                        array(
                          'label' => 'Salario Promedio',
                          'data' => $prom,
                          'lineTension' => 0 ,
                          'backgroundColor' => 'transparent',
                          'borderColor' => '#ff0000',
                          'borderWidth' => 2,
                          'pointBackgroundColor' => '#000000',
                          'type' => 'line'
                            )
                        );

             $data = array();
             array_push($data,
              array( 
                'labels' =>$labels,
                'datasets' => $datt
                  )
             );
            ?>             

         <canvas class="my-4" id="myChart" width="900" height="380"></canvas> 
         <?php }// fin del if ?>
         <?php if($opc == 3 ){ $type = "pie"; 
         $lab = array(); $dat = array(); $colors = array(); $data = array();?>
         <h2>Graficos de <?= $p  ?></h2>
         <?php foreach ($periodos as $periodo ) {  ?>
          <?php  if(count($periodo['data'])  > 0 ){ ?>
        
                      <?php $liquides  = 0; ?>
                      <?php foreach ($periodo['data'] as $valor ) { ?>
                      <?php
                        $liquides += $valor['liquides'];
                      ?>
                     <?php } ?>
                <?php 
                array_push($lab, $periodo['name']);
                array_push($colors, "#".$periodo['color']);
                array_push($dat, $liquides);

                ?>
          <?php } ?>
         <?php } ?>
         <?php 
         array_push($data,
            array( 
              'labels' => $lab,
              'datasets' => array(array(
                            'data' => $dat,
                            'backgroundColor'=>$colors,
                          ))
            )
          );
          ?>
         <canvas class="my-4" id="myChart" width="900" height="380"></canvas> 
         <?php }// fin del if ?>
         <?php } ?>
@endsection
