<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Dashboard Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../../../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../../../css/dashboard.css" rel="stylesheet">

    <link href="../../../../css/multi-select.css" rel="stylesheet">

    <!-- Include Date Range Picker -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
  </head>

  <body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Agence</a>
     <!--  <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">-->
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="#">Sign out</a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          @yield('menu')
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
         @yield('cuerpo')
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../js/jquery-slim.min.js"><\/script>')</script>
    <script src="../../../../js/popper.min.js"></script>
    <script src="../../../../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

    <script src="../../../../js/jquery.quicksearch.js"></script>
    <script src="../../../../js/jquery.multi-select.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>

    <!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>


    <script>
      feather.replace();
      $('#my-select').multiSelect({

selectableHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Buscar'>",
  selectionHeader: "<input type='text' class='form-control' autocomplete='off' placeholder='Buscar'>",

 afterInit: function(ms){
    var that = this,
        $selectableSearch = that.$selectableUl.prev(),
        $selectionSearch = that.$selectionUl.prev(),
        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
    .on('keydown', function(e){
      if (e.which === 40){
        that.$selectableUl.focus();
        return false;
      }
    });

    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
    .on('keydown', function(e){
      if (e.which == 40){
        that.$selectionUl.focus();
        return false;
      }
    });
  },


    keepOrder: true,
    afterSelect: function(values){
this.qs1.cache();
    this.qs2.cache();
 
    },
    afterDeselect: function(values){
this.qs1.cache();
    this.qs2.cache();
    },
  });

      $('input[name="daterange"]').daterangepicker(
      {
          locale: {
            format: 'YYYY-MM'
          },
      }
      );

      $("#r").click(function() {
        if($("#my-select" ).val() == ""){
          alert("Elegir al menos un consultor");
          return;
        }
        $("#opc" ).val(1);
        $("#form1" ).submit();
      });

      $("#g").click(function() {
        if($("#my-select" ).val() == ""){
          alert("Elegir al menos un consultor");
          return;
        }
        $("#opc" ).val(2);
        $("#form1" ).submit();
      });

      $("#p").click(function() {
        if($("#my-select" ).val() == ""){
          alert("Elegir al menos un consultor");
          return;
        }
        $("#opc" ).val(3);
        $("#form1" ).submit();
      });


    </script>
    <!-- Graphs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    <?php if($opc == 2  || $opc == 3 ){ ?>
    <script>
      var ctx = document.getElementById("myChart");
      var myChart = new Chart(ctx, {
        type: '<?= $type ?>',// bar, line, pie, doughnut
        data: <?= json_encode($data[0]); ?>,
        options: {
        responsive: true
        }
      });
    </script>
    <?php } ?>
  </body>
</html>
