<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salario extends Model{

	protected $table = 'cao_salario'
	
	protected $fillable = ['brut_salario']; 

}