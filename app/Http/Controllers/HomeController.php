<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {

        $sql = "SELECT * 
                FROM cao_usuario, permissao_sistema
                WHERE   cao_usuario.co_usuario = permissao_sistema.co_usuario
                    AND co_sistema = 1  
                    AND in_ativo = 'S'
                    AND co_tipo_usuario BETWEEN 0 AND 2";
        $rsp = \DB::select($sql);        
        
        $usuarios = array();              
        foreach ($rsp as $item) {
            array_push($usuarios ,  array(
            "co_usuario"=>$item->co_usuario,         
            "no_usuario"=>$item->no_usuario,         
            ));
        }    

        if ($request->isMethod('post')) {
           $diafinal = array(31,28,31,30,31,30,31,31,30,31,30,31);  
           $users = $request->input('usuarios');
           $opc = $request->input('opc');
           $periodo = $request->input('daterange');
           $fechas = explode(" - ", $periodo);
           
           $f1 = explode("-",$fechas[0]); 
           $f2 = explode("-",$fechas[1]); 
           $meses = (($f2[0] - $f1[0]) * 12 ) + $f2[1] - $f1[1] + 1;

           
           
           $respuesta = array();
            $bruto = 0;
           foreach ($users as $u) {
            $entro = 0;
           $ms = array();
            
$valores = array();
//$meses_array = array();
                 $sql = "SELECT 
                    cao_fatura.valor, 
                    cao_fatura.total_imp_inc,
                    cao_fatura.comissao_cn,
                    cao_salario.brut_salario
                FROM cao_fatura, cao_usuario, cao_os, cao_salario
                WHERE   cao_os.co_usuario = cao_usuario.co_usuario
                    AND cao_fatura.co_os = cao_os.co_os 
                    AND cao_salario.co_usuario = cao_usuario.co_usuario
                    AND cao_os.co_usuario = '".$u."' ";
                $mes = $f1[1];
            $anho = $f1[0];
        for ($i = 0; $i < $meses; $i++) {
         $cad = " AND cao_fatura.data_emissao BETWEEN '".$anho."-".$mes."-01' AND '".$anho."-".$mes."-".$diafinal[$mes-1]."'";
         

                       if($mes == 12){
                        $mes = 1;
                        $anho++;
                       }else{
                        $mes++;
                       }


                $rsp2 = \DB::select($sql.$cad); 
                $valores = array();
                $liquides =  0;
                $costo_fijo = 0;
                $comision = 0;
                $lucro = 0;    
                if( count($rsp2) > 0 ){

                    foreach ($rsp2 as $item) {
                        $liquides =  $liquides + ($item->valor * (1 - ($item->total_imp_inc/100)));
                        $costo_fijo = $costo_fijo + $item->brut_salario;
                        $comision = $comision +($liquides * ($item->comissao_cn / 100));
                        $lucro = $lucro + ($liquides - $costo_fijo - $comision); 

                        if($entro == 0){$bruto = $item->brut_salario;$entro = 1;}
                                      
                    }
                }
                        array_push($ms,array(
                                            'mes' =>$anho."-".$mes,
                                            'liquides' => $liquides,
                                            'costo_fijo' => $costo_fijo,
                                            'comision' => $comision,
                                            'lucro' => $lucro
                                            )
                                ); 
                 
                 

           
            }
            $color = str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT).str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT).str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
           array_push($respuesta, array('name' => $u, 'color' => $color,'salario' => $bruto,'data' =>$ms)); $bruto = 0;
        }


            return view('desempenho')->with('usuarios', $usuarios)->with('periodos', $respuesta)->with('opc', $opc)->with('p', $periodo)->with('seleccionados', $users);
        }else{
            return view('desempenho')->with('usuarios', $usuarios)->with('periodos', array())->with('opc', 0)->with('p', '')->with('seleccionados', []);
        }
    
                  
        
    }
}
